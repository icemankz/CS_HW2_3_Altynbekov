﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS_HW2_3_Altynbekov
{
    public class GeniusStudent : Student
    {
        public GeniusStudent(string name,
            string surname,
            int lessonQuantity)
            : base(name, surname, lessonQuantity)
        {

        }

        public override bool PassExam()
        {
            return LessonQuantity > 1;
        }

        public override int ShowMark()
        {          
            if (LessonQuantity > 1 && LessonQuantity < 21)
            {
                Console.WriteLine("Сдал!");
                return 1;
            }
            else
            {
                Console.WriteLine("Не сдал!");
                return 0;
            }
        }
    }
}
