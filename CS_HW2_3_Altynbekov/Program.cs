﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS_HW2_3_Altynbekov
{
    class Program
    {
        static void Main(string[] args)
        {

            List<Student> students = new List<Student>()
            {
                new OrdinaryStudent("Петя", "Петин", 17),
                new OrdinaryStudent("Вася", "Пупкин", 9),
                new OrdinaryStudent("Алина", "Апина", 16),
                new OrdinaryStudent("Дима", "Билан", 13),
                new OrdinaryStudent("Мария","Магдалена",20),
                new CleverStudent("Иван", "Ургант", 12),
                new CleverStudent("Шура", "Просто Шура", 15),
                new CleverStudent("Фифти", "Цент", 10),
                new CleverStudent("Таня", "Буланова", 19),
                new GeniusStudent("Илон", "Маск", 2)
            };
            foreach (var student in students)
            {
                student.Info();

            }
        }
    }
}
