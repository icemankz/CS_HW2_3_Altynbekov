﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS_HW2_3_Altynbekov
{

    public abstract class Student
    {
        public string Name;
        public string Surname;
        public int LessonQuantity;

        #region Constructors
        public Student() { }

        public Student(string name,
            string surname,
            int lessonQuantity)
        {
            Name = name;
            Surname = surname;
            LessonQuantity = lessonQuantity;
        }
        #endregion

        public void Info()
        {
            Console.WriteLine("Имя: {0}, Фамилия: {1}, Кол. посещенных уроков: {2} ", Name, Surname, LessonQuantity);
            ShowMark();
        }

        public virtual bool PassExam()
        {
            Random rand = new Random();
            double temp;
            temp = Convert.ToDouble(rand.Next(100)) / 100;
            return LessonQuantity > (temp);
        }

        public virtual int ShowMark()
        {
            if (PassExam())
            {
                Console.WriteLine("Сдал!");
                return 1;
            }
            else
            {
                Console.WriteLine("Не сдал!");
                return 0;
            }
        }
    }
}
