﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS_HW2_3_Altynbekov
{
    public class CleverStudent : Student
    {
        public CleverStudent(string name,
            string surname,
            int lessonQuantity)
            : base(name, surname, lessonQuantity)
        {
 
        }

        Random rand = new Random();
        double temp;

        public override bool PassExam()
        {
            if (LessonQuantity > 10)
            {
                temp = Convert.ToDouble(rand.Next(1000)) / 100;
                return LessonQuantity * 0.7 > (temp);
            }
            else
            {
                return false;
            }
        }

        public override int ShowMark()
        {
            int TotalLesson = 20;
            if ((LessonQuantity == TotalLesson) || (PassExam()))
            {
                Console.WriteLine("Сдал!");
                return 1;
            }
            else
            {
                Console.WriteLine("Не сдал!");
                return 0;
            }
        }
    }
}
